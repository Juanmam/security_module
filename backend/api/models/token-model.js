const mongoose = require('mongoose');


const tokenSchema = mongoose.Schema({
  iv: { type: Buffer, require: true },
  key: { type: Buffer, require: true }
});

module.exports = mongoose.model('Token', tokenSchema);
