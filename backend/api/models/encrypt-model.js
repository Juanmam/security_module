const mongoose = require('mongoose');

const encryptSchema = mongoose.Schema({
  secret: { type: String, require: true },
  size:   { type: Number, require: true},
  token:  { type: mongoose.Schema.ObjectId, require: true }
});

module.exports = mongoose.model('Encrypt', encryptSchema);
