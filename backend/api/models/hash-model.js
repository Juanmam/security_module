const mongoose = require('mongoose');

const hashSchema = mongoose.Schema({
  hash: { type: Buffer, require: true }
});

module.exports = mongoose.model('Hash', hashSchema);
