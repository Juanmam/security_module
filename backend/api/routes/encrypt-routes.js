// Requires
const express = require('express');
const router = express.Router();

// Models
const modelController = require('../controllers/encrypt-controller');

// Encryption
router.post('/', modelController.encrypt);

router.get('/all', modelController.getAll);

router.get('/decrypt', modelController.decrypt);

router.delete('/deleteAll', modelController.deleteAll)

module.exports = router;
