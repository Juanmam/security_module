// Requires
const express = require('express');
const router = express.Router();

// Models
const modelController = require('../controllers/hashing-controller');

// hashes a password using scrypt
router.post('/', modelController.hash);

//
router.get('/validate', modelController.validate);

//
router.get('/all', modelController.getAll);

//
router.delete('/deleteAll', modelController.deleteAll)

module.exports = router;
