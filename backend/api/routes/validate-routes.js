// Requires
const express = require('express');
const router = express.Router();

// Models
const modelController = require('../controllers/validate-controller');

module.exports = router;
