// Requires
const express = require('express');
const router = express.Router();

// Models
const modelController = require('../controllers/token-controller');

router.post('/', modelController.getToken);

router.get('/all', modelController.getAll);

router.get('/sesion', modelController.getSesionToken)

router.delete('/delete', modelController.deleteAll);

module.exports = router;
