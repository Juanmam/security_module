///REQUIRES
const mongoose = require('mongoose');
const crypto   = require('crypto');
const utf8     = require('utf8')

//MODELS
const Encrypt = require('../models/encrypt-model.js')
const Token   = require('../models/token-model.js')

// Variables
// We need to generate the key and iv and then store them on a Token
let iv  = crypto.randomBytes(16)
let key = crypto.randomBytes(32)

exports.getToken = (req,res, next) => {
  console.log('IV length of ' + iv.length);
  console.log('Key length of ' + key.length);
  // We create the Token
  Token.find({iv: iv, key: key}).then(result => {
    if(result.length == 0){
      let newToken = new Token({
        iv:  iv,
        key: key
      });

      // We save the token
      newToken.save().then(result => {
        if(res){
          res.status(200).json({
            result
          })
        } else {
          return result
        }
      }).catch(err => {
        if(res){
          res.status(500).json({
            err
          })
        } else {
          return err
        }
      });
    } else {
      res.status(200).json({
        result
      })
    }
  }).catch(err => {
    if(res){
      res.status(500).json({
        err
      });
    }
  });
}

exports.getSesionToken = (req, res, next) => {
  Token.findOne().sort({_id: -1}).limit(1).then(result => {
    res.status(200).json({
      result
    });
  }).catch(err => {
    res.status(500).json({
      err
    })
  });
}

exports.getAll = (req, res, next) => {
  Token.find({}).then(result => {
    res.status(200).json({
      result
    });
  }).catch(err => {
    res.status(500).json({
      err
    })
  })
}

exports.deleteAll = (req, res, next) => {
  Token.deleteMany({}).then(result => {
    res.status(200).json({
      result
    })
  }).catch(err => {
    res.status(500).json({
      err
    })
  })
}
