///REQUIRES
const mongoose = require('mongoose');
const scrypt   = require('scrypt');
const utf8     = require('utf8')

//MODELS
const Hash = require('../models/hash-model')

// Recives as a parameter a password pass and returns the hash
exports.hash = (req, res, next) => {
  let data = JSON.parse(req.body.data);
  let pass = new Buffer(data.pass)

  scrypt.hash(pass, {"N": 16, "r":1, "p":1},64,"")
  .then(result => {

    let newHash = new Hash({
      hash: result
    });

    newHash.save()
    .then(result => {
      res.status(200).json({
        result
      });
    })
    .catch(err => {
      res.status(500).json({
        Message: "Error creating new hash object.",
        err
      });
    })
  })
  .catch(err => {
    res.status(500).json({
      err
    });
  });
}

// Recives as a parameter a password pass and a hash id.
// Retruns true if both are the same.
exports.validate = (req, res, next) => {
  let data = JSON.parse(req.body.data);

  Hash.findOne({_id: data.id})
  .then(checkObject => {
    if(checkObject){
      scrypt.hash(new Buffer(data.pass), {"N": 16, "r":1, "p":1},64,"")
      .then(result => {

        let match = Buffer.from(result).toString('hex') === Buffer.from(checkObject.hash).toString('hex')
        console.log(match);

        res.status(200).json({
          result: match
        })
      })
      .catch(err => {
        res.status(500).json({
          err
        });
      });
    } else {
      // Object doesn't exist
      res.status(500).json({
        Message: "No object defined with that id."
      })
    }
  })
  .catch(err => {
      res.status(500).json({
        err
      })
  })
}

exports.getAll = (req, res, next) => {
  Hash.find({})
  .then(result => {
    res.status(200).json({
      result
    })
  })
  .catch(err => {
    res.status(500).json({
      err
    })
  })
}

exports.deleteAll = (req, res, next) => {
  Hash.deleteMany({})
  .then(result => {
    res.status(200).json({
      result
    })
  })
  .catch(err => {
    res.status(500).json({
      err
    })
  })
}
