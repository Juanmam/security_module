///REQUIRES
const mongoose = require('mongoose');
const crypto   = require('crypto');
const utf8     = require('utf8')

//MODELS
const Encrypt = require('../models/encrypt-model')
const Token   = require('../models/token-model')

exports.encrypt = (req, res, next) => {
  let data   = JSON.parse(req.body.data);

  Token.findOne().sort({_id: -1}).limit(1).then(sesion => {
    if(data.text != undefined){
      try{
        let iv  = sesion.iv;
        let key = sesion.key;

        // We encrypt
        let cipher    = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
        let encrypted = cipher.update(data.text);
        encrypted     = Buffer.concat([encrypted, cipher.final()]);
        let secret    = encrypted.toString('hex')

        console.log('input data length: ' + data.text.length);

        let newEncryption = new Encrypt({
          secret: secret,
          size:   data.text.length,
          token:  sesion
        })

        newEncryption.save().then(encrypted => {
          res.status(200).json({
            newEncryption
          })
        })

      } catch(error) {
        if(res){
          res.status(500).json({
            error
          })
        } else {
          console.log(error)
        }
      }
    }
  }).catch(err => {
    res.status(500).json({
      err
    })
  });


}

exports.decrypt = (req, res, next) => {
  let data = JSON.parse(req.body.data)

  Token.findOne().sort({_id: -1}).limit(1).then(sesion => {
    Encrypt.findOne({ "_id": data.id }).then(encrypt => {
      Token.findOne({_id: encrypt.token}).then(token => {
        if(encrypt.secret != undefined && token.iv != undefined){
          try{
            let iv = Buffer.from(token.iv);
            let key = Buffer.from(token.key)
            let secret = encrypt.secret.toString('hex')

            let encryptedText = Buffer.from(secret, 'hex', 'utf8');
            let decipher = crypto.createDecipheriv('aes-256-cbc', key, token.iv);
            decipher.setAutoPadding(false);
            let decrypted = decipher.update(encryptedText);//, 'hex', 'utf8');

            decrypted = Buffer.concat([decrypted, decipher.final()]);
            let result = decrypted.toString('utf8').slice(0, encrypt.size)

            console.log('decrypted output length: ' + result.length);
            console.log('iv ' + iv);
            console.log('key ' + key);
            console.log('secret ' + secret);
            console.log(result);

            res.status(200).json({
              result
            })
          } catch(error) {
            console.log(error)
          }
        } else {
          return ""
        }
      })
    }).catch(err => {
      res.status(500).json({
        err
      })
    })

  }).catch(err => {
    res.status(500).json({
      err
    })
  })
}

exports.getAll = (req, res, next) => {
  Encrypt.find({}).then(result => {
    res.status(200).json({
      result
    })
  })
}

exports.deleteAll = (req, res, next) => {
  Encrypt.deleteMany({}).then(result => {
    res.status(200).json({
      result
    })
  })
}
