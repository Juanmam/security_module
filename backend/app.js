// Requires
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const config = require('./config/config.js');

// Routes
encryptRoutes  = require('./api/routes/encrypt-routes')
hashingRoutes  = require('./api/routes/hashing-routes')
validateRoutes = require('./api/routes/validate-routes')
tokenRoutes    = require('./api/routes/token-routes')

// Base URL Routes
const encryptBaseRoute  = '/api/encrypt'
const hashingBaseRoute  = '/api/hash'
const validateBaseRoute = '/api/validate'
const tokenBaseRoute    = '/api/token'

app.all('*', function (req, res, next) {
  var origin = req.get('origin');
  res.header('Access-Control-Allow-Origin', origin);
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

if (!config.isTest){
  dbUser = '';
  dbPass = '';
  dbCluster = '';
  DB = 'TestEncrypt';
} else {
  dbUser = '';
  dbPass = '';
  dbCluster = '';
  DB = 'Encrypt';
}

mongoose.set('useFindAndModify', false);

// mongodb://127.0.0.1:27017/tugodb
mongoose.connect(`mongodb://127.0.0.1:27017/securitydb`, {
    useNewUrlParser: true,
    useFindAndModify: false
}).then(() => {
    console.log(`Conected to database: ${DB}`);
}).catch(error => {
    console.log(error);
});
mongoose.Promise = global.Promise;

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use((req, res, next) => {

  console.log("request arrived for URL", req.url);

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, DELETE, OPTIONS, PUT'
  );
  next();
});

// Encrypt Routes
app.use(`${encryptBaseRoute}`, encryptRoutes);


// Hashing Routes
app.use(`${hashingBaseRoute}`, hashingRoutes);

// Validate Routes
app.use(`${validateBaseRoute}`, validateRoutes);

// Token Routes
app.use(`${tokenBaseRoute}`, tokenRoutes);

//Some basic error handling
app.use((req, res, next) => {
    const error = new Error('Path Not Found');
    error.status = 404;
    next(error);
});

// Error handling for last unkown instance.
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    console.log("ERROR:", err);
    res.json({
        error: {
            status: err.status,
            error: "Error en el servidor",
            debug: "Ultima validacion de error"
        }
    });
});

// Export app
module.exports = app;
